package com.wu.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuyong
 * Description:
 * Created: sunny
 * Email: nengliang0816@outlook.com
 * Date: 2021-09-04 14:25
 */
@RestController
@RequestMapping("/api")
public class BookServiceController {

    @GetMapping("/testBook")
    public String getBookDemo(){

        return "wu";
    }
}
